package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()

	e.GET("/", func(c echo.Context) error {

		return c.String(http.StatusOK, "homepage")
	})

	e.GET("/version/", func(c echo.Context) error {

		return c.String(http.StatusOK, "version 1.0")
	})

	e.GET("/hello-world/", func(c echo.Context) error {

		return c.String(http.StatusOK, "Hello World")
	})

	e.Start(":8080")
}
