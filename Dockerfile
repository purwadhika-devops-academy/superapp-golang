FROM golang:1.14-stretch AS builder
WORKDIR /app
COPY . .
RUN CGO_ENABLED=0 go build -o application

FROM alpine:latest
WORKDIR /app
COPY --from=builder /app/application .

EXPOSE 8080

ENTRYPOINT ["/app/application"]